(() => {
  function createForm(container) {
    const form = document.createElement('form');
    const input = document.createElement('input');
    const button = document.createElement('button');

    form.classList.add('input-group', 'mb-3');
    input.classList.add('form-control');
    button.classList.add('btn', 'btn-primary');

    input.placeholder = 'Кол-во карточек по вертикали/горизонтали';
    button.textContent = 'Начать игру';

    form.append(input);
    form.append(button);

    return {
      form,
      input,
      button,
    };
  }

  function createArrayNumbers(length) {
    const arrayNumber = [];
    for (let i = 1; i <= length; i++) {
      arrayNumber.push(i);
      arrayNumber.push(i);
    }
    shuffle(arrayNumber);

    return arrayNumber;
  }

  function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

  function createGameField(container) {
    const field = document.createElement('table');

    field.classList.add('table');
    container.append(field);

    return field;
  }

  function createRowCard() {
    const rowCard = document.createElement(`tr`);
    rowCard.classList.add(`row-cards`);

    return rowCard;
  }

  function createCard(number) {
    const card = document.createElement('td');
    card.classList.add('card', 'm-3');

    card.textContent = number;

    return card;
  }

  function gameStart(container) {
    const gameForm = createForm(container);
    const gameField = createGameField(container);

    const MIN_AMOUNT_CARD = 2;
    const MAX_AMOUNT_CARD = 10;

    gameForm.form.addEventListener('submit', function(e) {
      e.preventDefault();
      let numberIsInput = parseInt(gameForm.input.value);

      if (numberIsInput >= MIN_AMOUNT_CARD && numberIsInput <= MAX_AMOUNT_CARD && numberIsInput % 2 === 0) {
        console.log('yes');
        const arrayNumbers = createArrayNumbers(numberIsInput);
        let currentCount = 0;
        for(let i = 1; i <= numberIsInput; i++) {
          const rowCard = createRowCard();
          gameField.append(rowCard);
          
          for(let j = 1; j <= numberIsInput; j++) {
            const card = createCard(arrayNumbers[currentCount++]);
            rowCard.append(card);
          }
        }
        
      } else {
        console.log('noooo');
      }

      gameForm.input.value = '';
    });

    container.append(gameForm.form);
    container.append(gameField);
  }

  gameStart(document.querySelector('.container'))

})();
